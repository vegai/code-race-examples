package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"
)

type Command struct {
	Throttle float64 `json:"throttle"` // Your target throttle value, 0-1
	Reverse  bool    `json:"reverse"`  // Set true if you want to reverse
	Brake    float64 `json:"brake"`    // Your target brake value, 0-1
	Steer    float64 `json:"steer"`    // Your target steering value, between -1 (left) and 1 (right)
}

type Vector struct {
	X       float64 `json:",string"`
	Y       float64 `json:",string"`
	IsValid bool
}

type Obstacle struct {
	Direction float64
	Distance  float64
	Size      float64 // largest dimension in world length units
	Kind      string
}

type Competitor struct {
	Direction float64
	Distance  float64
	Size      float64 // largest dimension in world length units
	Kind      string
}

type Response struct {
	Id                          int // your team id
	RaceId                      int
	RaceTick                    int     // current race tick value. use this to ignore possible old responses
	TimeElapsed                 float64 // seconds from start
	LapsCompleted               float64 // how many full laps you have completed
	CurrentLapElapsed           float64 // how many segments of current lap have you completed
	Throttle                    float64 // current throttle pedal position
	Brake                       float64 // current brake pedal position
	Steer                       float64 // current steering wheel position (-1 fully left, 1 fully right)
	NextWaypointDirection       float64
	NextWaypointDistance        float64
	NextCornerWaypointDirection float64
	NextCornerWaypointDistance  float64
	NextCornerDirection         string // which way the next corner turns? (Left/Right)
	AfterNextCornerDirection    string // how does the track continue after next corner? (Left/Right/Straight)
	SidewaysPosition            float64
	Velocity                    Vector  // velocity vector of the midpoint of the car
	Direction                   Vector  // this unit vector points to the direction of the car nose
	Acceleration                Vector  // acceleration (the driver feels) at the midpoint of the car
	FrontSlipAngle              float64 // angle between front wheel heading and actual direction of travel, always positive
	RearSlipAngle               float64 // angle between rear wheel heading and actual direction of travel, always positive
	Competitors                 map[int]Competitor
	Obstacles                   map[int]Obstacle
}

func main() {
	data, _ := ioutil.ReadFile("apikey")
	apiKey := strings.TrimSpace(string(data))
	log.Printf("Start your engines!")
	url := fmt.Sprintf("http://coderacewebapi.azurewebsites.net/api/v1/player-update/%s", apiKey)

	ticker := time.NewTicker(time.Millisecond * 200)
	go func() {
		for range ticker.C {
			command := Command{Throttle: 0.0, Reverse: false, Brake: 0.0, Steer: 0.0}
			response := doRequest(url, command)
			log.Printf("Race %d / Tick (%d)", response.RaceId, response.RaceTick)
		}
	}()
	select {
	// do nothing
	}
}

func doRequest(url string, command Command) Response {
	jsonStr, err := json.Marshal(command)
	log.Printf("%s", jsonStr)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	var response Response
	err = json.Unmarshal(body, &response)
	if err != nil {
		log.Print("Unable to parse message, error: " + err.Error())
	}

	return response
}
