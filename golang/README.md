# Getting Started with Go

1. Clone or copy repository: `git clone https://bitbucket.org/SC5/code-race-examples`
2. Navigate into src directory: `cd golang`
3. Put your API key in the `apikey` -file
4. Compile with `go build client.go` -command and then run the compiled binary
5. Create test race by clicking the `Create race` in [https://ui.coderace.sc5.io/](https://ui.coderace.sc5.io/) (Log in with your API key)
6. Start the race by clicking the `Start race` (once started, the car drives according given instructions)
7. ???
8. Profit!
