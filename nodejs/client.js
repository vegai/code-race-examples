'use strict';
require('es6-promise').polyfill();
require('isomorphic-fetch');
require('dotenv').config();

const chalk = require('chalk');
const async = require('asyncawait/async');
const await = require('asyncawait/await');

const drive = function(opts) {
  console.log(chalk.yellow('Starting the engine...'));
  let currentRaceId = 0;
  let currentRaceTick = 0;
  let currentStatus = {};

  setInterval(() => {
    const nextUpdate = getNextUpdate(currentStatus);
    console.log(chalk.gray('= tick:', currentStatus.RaceTick, '='));

    fetch(opts.url, {
      method: 'post',
      mode: 'cors',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(nextUpdate)
    })
    .then(response => (response.status === 200) ? response.json() : null)
    .then(sensoryData => {
      if (sensoryData) {
        if (sensoryData.RaceId !== currentRaceId) {
          currentRaceId = sensoryData.RaceId;
          currentRaceTick = 0;
        }

        if (sensoryData.RaceTick > currentRaceTick) {
          currentRaceTick = sensoryData.RaceTick;
          currentStatus = sensoryData;
          //console.log(currentStatus);
        }
      }
    })
    .catch(err => console.warn(chalk.orange(err)))
  }, opts.interval);
};

function getNextUpdate(sensoryData) {

  let update = {
    throttle: 0.5, // 0..1
    brake: 0, // 0..1
    steer: 0, // -1..1
    reverse: false // true/false
  };
  //
  // TODO: **Implement your logic here!**
  //

  return update;
}

// Get API key from https://ui.coderace.sc5.io/
const apiKey = process.env.API_KEY;
const url = `http://coderacewebapi.azurewebsites.net/api/v1/player-update/${apiKey}`;
const interval = 200;

if (apiKey) {
  console.log(chalk.green(`Starting engine ${apiKey}`));
} else {
  console.error(chalk.red('!API key missing!'), chalk.yellow('set value in .env'));
  process.exit(-2);
}

drive({ url, interval });
