package io.sc5.coderace;


import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.simple.JsonObject;
import org.json.simple.Jsoner;

import java.io.*;
import java.time.Duration;
import java.time.Instant;
import java.util.Properties;
import java.util.logging.Logger;

class Client {
    private static final Logger log = Logger.getLogger("Client");

    private static final int REQUEST_INTERVAL_MS = 200;
    private static final String URL = "http://coderacewebapi.azurewebsites.net/api/v1/player-update/";

    private static String apikey = null;

    private JsonObject getNextUpdate(JsonObject response) {
        // parse response here and create the new request payload
        JsonObject json = new JsonObject();
        json.put("throttle", 0.5f);
        json.put("brake", 0f);
        json.put("steer", 0.2f);
        json.put("reverse", false);
        return json;
    }

    public void run() throws IOException, InterruptedException {
        log.info("run");
        JsonObject response = null;
        while (true) {
            Instant before = Instant.now();
            JsonObject update = getNextUpdate(response);
            response = sendRequest(update);
            if (response.size() > 1) {
                log.info(String.format("= tick: %s =", response.getInteger("RaceTick")));
                Instant after = Instant.now();
                long exectime = Duration.between(before, after).toMillis();

                if (exectime < REQUEST_INTERVAL_MS) {
                    Thread.sleep(REQUEST_INTERVAL_MS - exectime);
                }
            } else {
                log.severe("empy response, maybe incorrect apikey?");
                Thread.sleep(REQUEST_INTERVAL_MS);
            }
        }
    }

    public JsonObject sendRequest(JsonObject json) throws IOException {
        HttpPost post = new HttpPost(URL + apikey);
        post.setHeader("Accept", "application/json");
        post.setHeader("Content-type", "application/json");
        String string = String.format("{\n" +
                "  \"throttle\": %f,\n" +
                "  \"brake\": %f,\n" +
                "  \"steer\": %f,\n" +
                "  \"reverse\": %s\n" +
                "}", json.getFloat("throttle"), json.getFloat("brake"), json.getFloat("steer"), json.getBoolean("reverse"));
        post.setEntity(new StringEntity(string));
        CloseableHttpClient client = HttpClients.createDefault();
        HttpResponse response = client.execute(post);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        client.close();
        return Jsoner.deserialize(result.toString(), new JsonObject());
    }

    public static void main(String[] args) throws IOException {
        File file = new File(".env");
        try {
            FileInputStream fileInput = new FileInputStream(file);
            Properties properties = new Properties();
            properties.load(fileInput);
            apikey = properties.getProperty("API_KEY");
            fileInput.close();
            log.info("Start the engine using apikey " + apikey);
            Client c = new Client();
            c.run();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
