# Getting Started with Java #

1. Clone or copy repository: `git clone https://bitbucket.org/SC5/code-race-examples my-car`
2. You should have received an API key from the race master,replace the placeholder key in .env file with that
3. Optional: Open the project my-car/java  in IntelliJ IDEA. you can run and debug code with it
4. If you dont wish to use idea, there is compile.sh to compile the code and run.sh to run it.
5. getNextUpdate(JsonObject response) receives the response and returns the next request payload (note first call to method is done with null value)
6. Run the client, it should now connect to the race you started and respond with RaceTick
7. Click "Start" in your browser, there is small buffer delay before race starts
8. Continue making the car smarter!
