## So how do I win?

Races are endurance style.  Winner is the car which has driven most full laps (and segments) when the racing time runs out.  To complete laps (and segments) the midpoint of the car has to visit all the track segments in order.

## Implementing the engine (advanced)

This chapter describes the sensory data and requests in more detail.  Also you get a few tips on how to leverage the data.

### Send updates

You can drive car by sending POST requests with 100ms interval to address:
`http://coderacewebapi.azurewebsites.net/api/v1/player-update/API_KEY`

Request body:
```
{
  "throttle": 0.7,
  "reverse": false,
  "brake": 0,
  "steer": 0.3
}
```

Where:

- **throttle**: Your target throttle value, 0-1
- **reverse**: Set `true` if you want to reverse
- **brake**: Your target brake value, 0-1
- **steer**: Your target steering value, between -1 (left) and 1 (right)

**PRO TIP:** You can tryout the API by sending requests manually at first, for example using [Postman](https://www.getpostman.com/)

### Receive sensory data (as response)

On each update request to the backend, you'll get a current sensory data as a response, which can utilize to make your car smarter.  There are a few general conventions that are good to know

- All the distances are measured in distance units of the physics engine.  You can think of them as meters.  The track is ~5.5m wide and cars are ~1.5m wide.
- All the angles are given in degrees.  Direction increasing angle is clockwise.
- Origin of the world coordinates is at bottom left corner of the screen.
- All the directions are given relative to the car orientation.  Range is -180 to 180.  Direction 0 is where the car nose is pointing, 180 is directly behind the car and -90 is on the left.
- All the vectors are given in world coordinates, i.e. independent of the heading of the car.

```
{
  "Id": 8, -- your team id
  "RaceId": 157, -- current race id
  "RaceTick": 7139, -- current race tick value. use this to ignore old responses
  "TimeElapsed": 35.30999921076, -- seconds from start
  "LapsCompleted": 0, -- how many full laps you have completed
  "CurrentLapElapsed": 1, -- how many segments of current lap have you completed
  "Throttle": 0.7, -- current throttle pedal position
  "Brake": 0.1, -- current brake pedal position
  "Steer": -0.2, -- current steering wheel position (-1 fully left, 1 fully right)
  "nextWaypointDirection": -71.769492696684509,
  "nextWaypointDistance": 44.021400451660156,
  "nextCornerWaypointDirection": -14.390294910020799,
  "nextCornerWaypointDistance": 53.895420074462891,
  "nextCornerDirection": "Left", -- which way the next corner turns?
  "afterNextCornerDirection": "Straight", -- how does the track continue after next corner?
  "Velocity": { -- velocity vector of the midpoint of the car
    "X": -0.6580646,
    "Y": -0.102406994
  },
  "Direction": { -- this unit vector points to the direction of the car nose
    "X": -0.9606035,
    "Y": -0.27792266
  },
  "Acceleration": { -- acceleration (the driver feels) at the midpoint of the car
    "X": 4.148163,
    "Y": 1.85572994
  },
  "FrontSlipAngle": 2.15302062072884037, -- angle between front wheel heading and actual direction of travel, always positive
  "RearSlipAngle": 0.6446452601781269, -- angle between rear wheel heading and actual direction of travel, always positive
  "Competitors": {
    10: { -- competitor's team id
      "Direction": -35.390294910020799,
      "Distance": 8.021400451660156,
      "Size": 3.24446452601781269, -- largest dimension in world length units
      "Kind": "Car"
    }
  },
  "Obstacles": {
    1: { -- obstacle id
      "Direction": -35.390294910020799,
      "Distance": 8.021400451660156,
      "Size": 3.24446452601781269, -- largest dimension in world length units
      "Kind": "Tree"
    }
  }
}
```

## Tips

- `nextWaypointDirection` is where you should be heading to make progress in race.
- `nextCornerWaypointDirection` points towards the apex of the next corner (and is equal to `nextWaypointDirection` if the next track segment is a corner).
- For straight track segments the waypoint is located right in the middle of the segment (both ways) and for corners in the inside of the curve, half way thru it (see the illustrations)
- You should avoid sliding too much.  Aim to keep `FrontSlipAngle` and `RearSlipAngle` between -5 and 5.
- Generally only competitors and obstacles near you (small distance) and in front of your (small direction angle) are of interest.
- These racing cars don't do particularily well in off road conditions.
- Steering wheel does not react immediately.  If you flip `steer` in your update request from -1 to 1 in an instant, it takes a little time for `Steer` in sensory data to reach 1.
- All the obstacles are either immovable or significantly heavier than your car.  It is not a good idea ty try to plow thru them.
- To complete a lap, you need to visit all the track segments in order.  Driving back and forth over the start line doesn't quite cut it.  Sorry about that.

## Sensory data explained

Following illustrations explains some of sensory data sources:

<img src="https://bitbucket.org/SC5/code-race-examples/raw/HEAD/sensory.svg" alt="Sensory data" style="width: 100%; max-width: 750px" width="750px" />

**NOTE:** the origo exists in the car center. The value `X` increases from left to right, and `Y` from top to bottom. Angle 0 is front of the car, value being between -180..180 (left...right).
