# Getting Started with Python

1. Clone or copy repository: `git clone https://bitbucket.org/SC5/code-race-examples my-car`
2. Navigate into src directory: `cd my-car/python/`
3. Install dependencies: `pip install -r requirements.txt`
4. Set `API_KEY = $API_KEY` in `.env`
5. Start engine with defaults: `python client.py`
6. Create test race by clicking the `Create race` in [https://ui.coderace.sc5.io/](https://ui.coderace.sc5.io/) (if you have engine running, the car should appear in the map)
7. Start the race by clicking the `Start race`

## Implementing the engine (simple)

1. Start editing `get_next_update(sensory_data)` in `client.py`. Remember to restart the engine after each edit.
2. Sensory data gets updated after each request. Calculate the next update values by using the sensory data. Especially, You might want to start with `nextWaypoint`. Direction is degrees from -180 to 180, relative to your car. Zero is forward, -90 at your left:
```
{
  "nextWaypointDirection": -71.769492696684509, -- The direction You should be going to
  "nextWaypointDistance": 44.021400451660156,
  ...
}
```
3. Click the flag in the map to restart the race
4. Rinse and repeat!

<img src="https://bitbucket.org/SC5/code-race-examples/raw/HEAD/summary.svg" alt="Sensory data" style="width: 100%; max-width: 400px" width="400px" />
