#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
# Setup

- Set API key in .env file
- Install deps: pip install -r requirements.txt
- Start client: python client.py

'''
import sys
import os
import time
import logging
from threading import Timer
from os.path import join, dirname
from dotenv import load_dotenv
import requests

update = {
  'throttle': 0.5,
  'brake': 0.1,
  'steer': 0,
  'reverse': False,
}


class Car:
  def __init__(self):
    self.sensory_data = {}

  def send_update(self):
    update = self.get_next_update(self.sensory_data)
    print('== tick: ', self.sensory_data.get('RaceTick', '--'), '==')
    r = requests.post(url, json=update)

    if r.status_code != 200:
      logging.warn('Non-ok response')
    else:
      self.sensory_data = r.json()

  def drive(self, url, interval):
    logging.info('Starting the car...')
    while True:
      t = Timer(interval, self.send_update)
      t.start()
      t.join()

  def get_next_update(self, sensory_data):
    logging.debug(sensory_data)
    #
    # TODO: Implement me!
    #

    return update


dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

api_key = os.environ.get('API_KEY')

if not api_key:
  logging.error('API key missing! Set value in .env')
  sys.exit(-1)

url = 'http://coderacewebapi.azurewebsites.net/api/v1/player-update/' + api_key;
interval = 0.200

car = Car()
car.drive(url=url, interval=interval)
