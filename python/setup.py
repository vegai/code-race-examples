
from distutils.core import setup

setup(name='code-race-example-python',
 version='0.1',
 description='Simple boilerplate for Code Race',
 author='SC5',
 author_email='info@sc5.io',
 url='https://sc5.io/',
 packages=['requests','python-dotenv']
)
