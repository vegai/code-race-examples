# Getting Started with C\# #

1. Clone or copy repository: `git clone https://bitbucket.org/SC5/code-race-examples my-car`
2. Open the project file from my-car/dotnet/Client folder in Visual Studio
3. Build solution to refresh packages (only dependency what is used is Newtonsoft.Json)
4. You should have received an API key from the race master, place that inside const named API_KEY at the beging of the Program.cs
5. Create a race at http://ui.coderace.sc5.io/#/home (login with your API key)
6. Start the console app, it should now connect to the race you started and respond with RaceId + RaceTick
7. Click "Start" in your browser, there is small buffer delay before race starts
8. Continue making the car smarter!
